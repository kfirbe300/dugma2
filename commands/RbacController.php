<?php
namespace app\commands;

use Yii;
use yii\console\Controller;
use \app\rbac\AuthorRule;


class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;
        $rule = new AuthorRule;
        $auth->add($rule);
    }
}